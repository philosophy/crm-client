<?php
namespace OurPhilosophy\TalentBake\CrmClient;

use CommerceGuys\Guzzle\Oauth2\GrantType\ClientCredentials;
use CommerceGuys\Guzzle\Oauth2\GrantType\PasswordCredentials;
use CommerceGuys\Guzzle\Oauth2\GrantType\RefreshToken;
use CommerceGuys\Guzzle\Oauth2\Oauth2Subscriber;
use GuzzleHttp\Client;

/**
 * Created by IntelliJ IDEA.
 * User: kieran
 * Date: 10/05/16
 * Time: 09:42
 */

class Api {

	const GRANT_CLIENT_CREDENTIALS = 'client_credentials';
	const GRANT_PASSWORD =  'password';
	const GRANT_CANDIDATE_PASSWORD = 'candidate_password';

	/**
	 * @var Client
	 */
	protected $oauth;

	protected $baseUri = 'http://192.168.99.100/';

	protected $apiVersion = '1.0';

	protected $client;

	/**
	 * @var array
	 */
	protected $config;

	/**
	 *
	 */
	public function __construct(array $config)
	{
		if (!array_key_exists('client_id', $config)) {
			throw new \Exception('Missing client_id');
		}

		if (!array_key_exists('grant_type', $config)) {
			throw new \Exception('Missing grant_type');
		}

		if ($config['grant_type'] == self::GRANT_CLIENT_CREDENTIALS &&
			!array_key_exists('client_secret', $config)) {

			throw new \Exception('This grant type requires the client secret');
		}

		if ($config['grant_type'] == self::GRANT_PASSWORD &&
			(!array_key_exists('username', $config) || !array_key_exists('password', $config))) {

			throw new \Exception('Missing username|password for this grant type');
		}

		if (array_key_exists('base_url', $config)) {
			$this->baseUri = $config['base_url'];
		}

		// TODO: implement oauth real-flow (this library should not be public!)
		$this->config = $config;
	}

	/**
	 *
	 */
	public function authenticate()
	{
		$this->oauth = new Client(['base_url' => $this->baseUri]);

		$config = [];

		if ($this->config['grant_type'] == self::GRANT_CLIENT_CREDENTIALS) {
			$config['client_id'] = $this->config['client_id'];
			$config['client_secret'] = $this->config['client_secret'];
			$config['token_url'] = 'oauth/access_token';
			// INstance
			$token = new ClientCredentials($this->oauth, $config);
		}

		if ($this->config['grant_type'] == self::GRANT_PASSWORD) {
			$config['client_id'] = $this->config['client_id'];
			$config['username'] = $this->config['username'];

			$config['token_url'] = 'oauth/access_token';
			$config['password'] = $this->config['password'];

			// instance
			$token = new PasswordCredentials($this->oauth, $config);
		}

		//$refreshToken = new RefreshToken($this->oauth, $config);
		$this->oauth = new Oauth2Subscriber($token);
	}

	/**
	 *
	 */
	public function setup()
	{
		$this->authenticate();
		$this->client = new Client([
			'base_url' => $this->baseUri,
			'defaults' => [
				'auth' => 'oauth2',
				'subscribers' => [$this->oauth],
			],
		]);
	}

	/**
	 * @param $uri
	 * @return mixed
	 */
	public function get($uri)
	{
		$response = $this->client->get($uri);
		return $response;
	}

	public function getClient()
	{
		return $this->client;
	}
} 